package com.cknox_capstone.AutoMagic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class WifiTimerReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        SharedPreferences preferences = context.getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        boolean wifiMode = preferences.getBoolean("WifiMode", false);

        ((WifiManager)context.getSystemService(Context.WIFI_SERVICE)).setWifiEnabled(wifiMode);
        Toast.makeText(context, "Wifi Back On", Toast.LENGTH_SHORT).show();
    }
}
