package com.cknox_capstone.AutoMagic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;

import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class CallBlockReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        SharedPreferences preferences = context.getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        boolean blockCalls = preferences.getBoolean("BlockCalls", false);

        if(blockCalls)
        {
            Bundle bundle = intent.getExtras();
            if(bundle != null)
            {
                try{
                    if(intent.getAction().equals("android.intent.action.PHONE_STATE"))
                    {
                        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                        if(state.equals(TelephonyManager.EXTRA_STATE_RINGING))
                        {
                            TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
                            Class<?> telephonyClass = Class.forName(telephonyManager.getClass().getName());
                            Method methodGetITelephony = telephonyClass.getDeclaredMethod("getITelephony");
                            methodGetITelephony.setAccessible(true);

                            Object telephonyInterface = methodGetITelephony.invoke(telephonyManager);
                            Class<?> telephonyInterfaceClass = Class.forName(telephonyInterface.getClass().getName());
                            Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");
                            methodEndCall.invoke(telephonyInterface);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}
