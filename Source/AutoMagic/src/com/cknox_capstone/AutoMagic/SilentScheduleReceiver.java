package com.cknox_capstone.AutoMagic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class SilentScheduleReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        boolean isStart = intent.getBooleanExtra("isAlarmStart", true);
        if(isStart)
        {
            silencePhone(context);
        }
        else
        {
            unSilencePhone(context);
        }
    }

    private void silencePhone(Context context)
    {
        AudioManager audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
        saveRingerMode(context, audioManager.getRingerMode());

        audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
    }

    private void saveRingerMode(Context context, int ringerMode)
    {
        SharedPreferences preferences = context.getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("ScheduleRingerMode", ringerMode);
        editor.commit();
    }

    private void unSilencePhone(Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        int ringerMode = preferences.getInt("ScheduleRingerMode", AudioManager.RINGER_MODE_NORMAL);

        ((AudioManager)context.getSystemService(Context.AUDIO_SERVICE)).setRingerMode(ringerMode);
    }
}
