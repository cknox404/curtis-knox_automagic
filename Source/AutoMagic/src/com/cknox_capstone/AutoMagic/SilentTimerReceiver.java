package com.cknox_capstone.AutoMagic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class SilentTimerReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        SharedPreferences preferences = context.getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        int ringerMode = preferences.getInt("RingerMode", AudioManager.RINGER_MODE_NORMAL);

        ((AudioManager)context.getSystemService(Context.AUDIO_SERVICE)).setRingerMode(ringerMode);
        Toast.makeText(context, "Phone Unsilenced", Toast.LENGTH_SHORT).show();
    }
}
