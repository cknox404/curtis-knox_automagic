package com.cknox_capstone.AutoMagic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;

import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class DataTimerReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        SharedPreferences preferences = context.getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        boolean dataMode = preferences.getBoolean("TimerDataMode", true);

        try{
            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            Method dataMethod = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
            dataMethod.setAccessible(true);
            dataMethod.invoke(connectivityManager, dataMode);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
