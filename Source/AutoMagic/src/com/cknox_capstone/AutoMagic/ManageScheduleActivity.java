package com.cknox_capstone.AutoMagic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class ManageScheduleActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_list);

        displayAlarms();
    }

    private void displayAlarms()
    {
        ArrayList<Alarm> alarms = getAlarms();

        if(alarms!=null)
        {
            ListView alarmList = (ListView)findViewById(R.id.alarmList);

            ArrayAdapter<Alarm> adapter = new ArrayAdapter<Alarm>(this,
                    android.R.layout.simple_list_item_1, alarms);

            alarmList.setAdapter(adapter);
            alarmList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Alarm selectedAlarm = (Alarm)parent.getItemAtPosition(position);
                    Intent intent = new Intent(ManageScheduleActivity.this, ManageIndividualScheduleActivity.class);
                    intent.putExtra("alarm", selectedAlarm);

                    startActivity(intent);
                }
            });
        }
    }

    public ArrayList<Alarm> getAlarms()
    {
        ArrayList<Alarm> alarms;
        try{
            FileInputStream fileIn = openFileInput(ScheduleActivity.ALARM_FILE);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            alarms = (ArrayList<Alarm>)objectIn.readObject();

            objectIn.close();
            fileIn.close();
        }catch (Exception e){
            alarms = null;
        }
        return alarms;
    }
}