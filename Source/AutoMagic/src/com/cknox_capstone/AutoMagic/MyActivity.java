package com.cknox_capstone.AutoMagic;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.GregorianCalendar;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public void scheduleAlarm(View v)
    {
        long time = new GregorianCalendar().getTimeInMillis()+30*1000;

        Intent intent = new Intent(this, AlarmReceiver.class);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, time,
                PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT));

        Toast.makeText(this, "Alarm Scheduled", Toast.LENGTH_LONG).show();
    }

    public void createSilentTimer(View v)
    {
        Intent intent = new Intent(this, SilentTimerActivity.class);
        startActivity(intent);
    }

    public void createWifiTimer(View v)
    {
        Intent intent = new Intent(this, WifiTimerActivity.class);
        startActivity(intent);
    }

    public void createDataTimer(View v)
    {
        Intent intent = new Intent(this, DataTimerActivity.class);
        startActivity(intent);
    }

    public void createSchedule(View v)
    {
        Intent intent = new Intent(this, ScheduleActivity.class);
        startActivity(intent);
    }

    public void manageSchedules(View v)
    {
        Intent intent = new Intent(this, ManageScheduleActivity.class);
        startActivity(intent);
    }

    public void enterDrivingMode(View v)
    {
        Intent intent = new Intent(this, DrivingModeActivity.class);
        startActivity(intent);
    }
}
