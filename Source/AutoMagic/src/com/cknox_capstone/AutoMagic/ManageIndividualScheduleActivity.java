package com.cknox_capstone.AutoMagic;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class ManageIndividualScheduleActivity extends Activity {

    Alarm alarm;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_schedule);

        alarm = (Alarm)getIntent().getSerializableExtra("alarm");
        TimePicker startTime = (TimePicker)findViewById(R.id.editStartTime);
        TimePicker endTime = (TimePicker)findViewById(R.id.editEndTime);

        startTime.setIs24HourView(true);
        endTime.setIs24HourView(true);

        startTime.setCurrentHour(alarm.getStartHour());
        startTime.setCurrentMinute(alarm.getStartMinute());
        endTime.setCurrentHour(alarm.getEndHour());
        endTime.setCurrentMinute(alarm.getEndMinute());
    }

    public void deleteSchedule(View v)
    {
        ArrayList<Alarm> alarms = getAlarms();
        HashMap<Integer, Integer> pendingIntentIds = getPendingIntentIds();
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        int startKey = (alarm.getStartHour()*60)+alarm.getStartMinute();
        int endKey = (alarm.getEndHour()*60)+alarm.getEndMinute();

        for(int day : alarm.getDays())
        {
            for(AlarmType alarmType : alarm.getAlarmTypes())
            {
                Intent intent = new Intent();
                int typeValue = 0;
                switch (alarmType)
                {
                    case SILENT:
                        intent = new Intent(this, SilentScheduleReceiver.class);
                        typeValue = AlarmType.SILENT.getTypeValue();
                        break;
                    case WIFI_OFF:
                        intent = new Intent(this, WifiScheduleReceiver.class);
                        typeValue = AlarmType.WIFI_OFF.getTypeValue();
                        break;
                    case DATA_OFF:
                        intent = new Intent(this, DataScheduleReceiver.class);
                        typeValue = AlarmType.DATA_OFF.getTypeValue();
                        break;
                }
                int key = typeValue+(day*24*60)+startKey;
                int id = pendingIntentIds.get(key);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                alarmManager.cancel(pendingIntent);
                pendingIntentIds.remove(key);

                key = typeValue+(day*24*60)+endKey;
                id = pendingIntentIds.get(key);
                pendingIntent = PendingIntent.getBroadcast(this, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                alarmManager.cancel(pendingIntent);
                pendingIntentIds.remove(key);
            }

        }

        for(Alarm a : alarms)
        {
            if(a.toString().equals(alarm.toString()))
            {
                alarms.remove(a);
            }
        }
        savePendingIntents(pendingIntentIds);
        saveAlarms(alarms);
        Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ManageScheduleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void saveChanges(View v)
    {
        TimePicker startTime = (TimePicker)findViewById(R.id.editStartTime);
        TimePicker endTime = (TimePicker)findViewById(R.id.editEndTime);

        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
    }

    private ArrayList<Alarm> getAlarms()
    {
        ArrayList<Alarm> alarms;
        try{
            FileInputStream fileIn = openFileInput(ScheduleActivity.ALARM_FILE);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            alarms = (ArrayList<Alarm>)objectIn.readObject();

            objectIn.close();
            fileIn.close();
        }catch (Exception e){
            alarms = null;
        }
        return alarms;
    }

    private HashMap<Integer, Integer> getPendingIntentIds()
    {
        HashMap<Integer, Integer> pendingIntents;
        try{
            FileInputStream fileIn = openFileInput(ScheduleActivity.PENDING_INTENT_FILE);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            pendingIntents = (HashMap<Integer, Integer>)objectIn.readObject();

            objectIn.close();
            fileIn.close();
        }catch (Exception e){
            pendingIntents = new HashMap<Integer, Integer>();
        }
        return pendingIntents;
    }

    private void savePendingIntents(HashMap<Integer, Integer> pendingIntentIds)
    {
        try{
            FileOutputStream fileOut = openFileOutput(ScheduleActivity.PENDING_INTENT_FILE, MODE_PRIVATE);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(pendingIntentIds);

            objectOut.close();
            fileOut.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void saveAlarms(ArrayList<Alarm> alarms)
    {
        try{
            FileOutputStream fileOut = openFileOutput(ScheduleActivity.ALARM_FILE, MODE_PRIVATE);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(alarms);

            objectOut.close();
            fileOut.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}