package com.cknox_capstone.AutoMagic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class DrivingModeActivity extends Activity {

    private SmsReceiver smsReceiver;
    private CallBlockReceiver callBlockReceiver;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driving_mode);
        smsReceiver = new SmsReceiver();
        callBlockReceiver = new CallBlockReceiver();
        addMessageText();

        silencePhone();
        registerReceiver(smsReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        registerReceiver(callBlockReceiver, new IntentFilter("android.intent.action.PHONE_STATE"));
    }

    private void addMessageText()
    {
        EditText editText = (EditText)findViewById(R.id.response_message);
        editText.setText(getResponseMessage(this));
        editText.setSelectAllOnFocus(true);
    }

    public static String getResponseMessage(Context context)
    {
        String message;
        SharedPreferences preferences = context.getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        message = preferences.getString("ResponseMessage",
                "I am currently driving and cannot properly respond to your message. I will respond to your message as soon as I can.");
        return message;
    }

    public void editMessage(View v)
    {
        EditText editText = (EditText)findViewById(R.id.response_message);
        String message = editText.getText().toString();

        SharedPreferences preferences = getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("ResponseMessage", message);
        editor.commit();

        Toast.makeText(this, "Message changed", Toast.LENGTH_LONG).show();
    }

    private void silencePhone()
    {
        AudioManager audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        saveRingerMode(audioManager.getRingerMode());

        audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
    }

    private void saveRingerMode(int ringerMode)
    {
        SharedPreferences preferences = getSharedPreferences(SilentTimerActivity.MY_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("DrivingRingerMode", ringerMode);
        editor.commit();
    }

    public void quitDrivingMode(View v)
    {
        unregisterReceiver(smsReceiver);
        unregisterReceiver(callBlockReceiver);
        unSilencePhone();
        Intent intent = new Intent(this, MyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void onBlockCallsBoxClicked(View v)
    {
        boolean checked = ((CheckBox)v).isChecked();
        SharedPreferences preferences = getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("BlockCalls", checked);
        editor.commit();
    }

    private void unSilencePhone()
    {
        SharedPreferences preferences = getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        int ringerMode = preferences.getInt("ScheduleRingerMode", AudioManager.RINGER_MODE_NORMAL);

        ((AudioManager)getSystemService(Context.AUDIO_SERVICE)).setRingerMode(ringerMode);
    }
}