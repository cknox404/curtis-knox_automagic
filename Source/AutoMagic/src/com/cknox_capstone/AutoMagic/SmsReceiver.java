package com.cknox_capstone.AutoMagic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class SmsReceiver extends BroadcastReceiver {

    private final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(SMS_RECEIVED))
        {
            Bundle bundle = intent.getExtras();
            if(bundle != null)
            {
                Object[] messages = (Object[])bundle.get("pdus");
                SmsMessage[] smsMessage = new SmsMessage[messages.length];
                for(int i = 0; i < messages.length; i++)
                {
                    smsMessage[i] = SmsMessage.createFromPdu((byte[])messages[i]);
                }
                String sender = smsMessage[0].getOriginatingAddress();

                String message = DrivingModeActivity.getResponseMessage(context);
                SmsManager.getDefault().sendTextMessage(sender, null, message, null, null);
            }
        }
    }
}
