package com.cknox_capstone.AutoMagic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class WifiScheduleReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        boolean isStart = intent.getBooleanExtra("isAlarmStart", true);
        if(isStart)
        {
            wifiOff(context);
        }
        else
        {
            wifiOn(context);
        }
    }

    private void wifiOff(Context context)
    {
        WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        saveWifiMode(context, wifiManager.isWifiEnabled());
        wifiManager.setWifiEnabled(false);
    }

    private void wifiOn(Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        boolean wifiMode = preferences.getBoolean("ScheduleWifiMode", false);

        ((WifiManager)context.getSystemService(Context.WIFI_SERVICE)).setWifiEnabled(wifiMode);
    }

    private void saveWifiMode(Context context, boolean wifiMode)
    {
        SharedPreferences preferences = context.getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("ScheduleWifiMode", wifiMode);
        editor.commit();
    }
}
