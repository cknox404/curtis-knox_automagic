package com.cknox_capstone.AutoMagic;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.util.GregorianCalendar;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class WifiTimerActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.silent_timer);

        createNumberPickers();
    }

    private void createNumberPickers()
    {
        NumberPicker hourPicker = (NumberPicker) findViewById(R.id.hours);
        hourPicker.setMinValue(0);
        hourPicker.setMaxValue(23);

        NumberPicker minutePicker = (NumberPicker) findViewById(R.id.minutes);
        minutePicker.setMinValue(0);
        minutePicker.setMaxValue(59);
    }

    public void startTimer(View v)
    {
        turnWifiOff();

        NumberPicker hourPicker = (NumberPicker) findViewById(R.id.hours);
        int hours = hourPicker.getValue();

        NumberPicker minutePicker = (NumberPicker) findViewById(R.id.minutes);
        int minutes = minutePicker.getValue();

        long time = new GregorianCalendar().getTimeInMillis()+getHoursInMillis(hours)+getMinutesInMillis(minutes);
        Intent intent = new Intent(this, WifiTimerReceiver.class);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(
                this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        );

        String message = "Wifi off for "+hours+" hours and "+minutes+" minutes";
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void turnWifiOff()
    {
        WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        saveWifiMode(wifiManager.isWifiEnabled());

        wifiManager.setWifiEnabled(false);

    }

    private int getHoursInMillis(int hours)
    {
        return hours*60*60*1000;
    }

    private int getMinutesInMillis(int minutes)
    {
        return minutes*60*1000;
    }

    private void saveWifiMode(boolean wifiMode)
    {
        SharedPreferences preferences = getSharedPreferences(SilentTimerActivity.MY_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("WifiMode", wifiMode);
        editor.commit();
    }
}