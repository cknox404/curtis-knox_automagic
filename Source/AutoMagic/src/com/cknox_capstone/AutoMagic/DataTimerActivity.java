package com.cknox_capstone.AutoMagic;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.GregorianCalendar;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class DataTimerActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.silent_timer);

        createNumberPickers();
    }

    private void createNumberPickers()
    {
        NumberPicker hourPicker = (NumberPicker) findViewById(R.id.hours);
        hourPicker.setMinValue(0);
        hourPicker.setMaxValue(23);

        NumberPicker minutePicker = (NumberPicker) findViewById(R.id.minutes);
        minutePicker.setMinValue(0);
        minutePicker.setMaxValue(59);
    }

    public void startTimer(View v)
    {
        turnDataOff();

        NumberPicker hourPicker = (NumberPicker) findViewById(R.id.hours);
        int hours = hourPicker.getValue();

        NumberPicker minutePicker = (NumberPicker) findViewById(R.id.minutes);
        int minutes = minutePicker.getValue();

        long time = new GregorianCalendar().getTimeInMillis()+getHoursInMillis(hours)+getMinutesInMillis(minutes);
        Intent intent = new Intent(this, DataTimerReceiver.class);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(
                this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        );

        String message = "Data off for "+hours+" hours and "+minutes+" minutes";
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void turnDataOff()
    {
        saveDataMode();
        try{
            ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            Method dataMethod = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
            dataMethod.setAccessible(true);
            dataMethod.invoke(connectivityManager, false);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private int getHoursInMillis(int hours)
    {
        return hours*60*60*1000;
    }

    private int getMinutesInMillis(int minutes)
    {
        return minutes*60*1000;
    }

    private void saveDataMode()
    {
        boolean dataMode = true;
        try{
            ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            Method method = ConnectivityManager.class.getDeclaredMethod("getMobileDataEnabled");
            dataMode = (Boolean)method.invoke(connectivityManager);
        }catch (Exception e){
            e.printStackTrace();
        }
        SharedPreferences preferences = getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("TimerDataMode", dataMode);
        editor.commit();
    }
}