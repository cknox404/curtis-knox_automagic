package com.cknox_capstone.AutoMagic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;

import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class DataScheduleReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        boolean isStart = intent.getBooleanExtra("isAlarmStart", true);
        if(isStart)
        {
            dataOff(context);
        }
        else
        {
            dataOn(context);
        }
    }

    private void dataOff(Context context)
    {
        saveDataMode(context);
        try{
            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            Method dataMethod = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
            dataMethod.setAccessible(true);
            dataMethod.invoke(connectivityManager, false);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void dataOn(Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        boolean dataMode = preferences.getBoolean("ScheduleDataMode", false);
        try{
            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            Method dataMethod = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
            dataMethod.setAccessible(true);
            dataMethod.invoke(connectivityManager, dataMode);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void saveDataMode(Context context)
    {
        boolean dataMode = true;
        try{
            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            Method method = ConnectivityManager.class.getDeclaredMethod("getMobileDataEnabled");
            dataMode = (Boolean)method.invoke(connectivityManager);
        }catch (Exception e){
            e.printStackTrace();
        }
        SharedPreferences preferences = context.getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("ScheduleDataMode", dataMode);
        editor.commit();
    }
}
