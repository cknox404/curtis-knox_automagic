package com.cknox_capstone.AutoMagic;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public enum AlarmType {
    SILENT("Silent", 100000),
    WIFI_OFF("Wifi off", 200000),
    DATA_OFF("Mobile data off", 300000),
    WIFI_ON("Wifi on", 400000),
    DATA_ON("Mobile data on", 500000);

    private final String name;
    private final int typeValue;

    private AlarmType(String name, int typeValue)
    {
        this.name = name;
        this.typeValue = typeValue;
    }

    public boolean equalsName(String name)
    {
        return this.name.equalsIgnoreCase(name);
    }

    public String getName()
    {
        return name;
    }

    public int getTypeValue()
    {
        return typeValue;
    }
}
