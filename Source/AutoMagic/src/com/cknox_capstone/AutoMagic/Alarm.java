package com.cknox_capstone.AutoMagic;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class Alarm implements Serializable {

    private ArrayList<Integer> days;
    private ArrayList<AlarmType> alarmTypes;
    private int startHour;
    private int endHour;
    private int startMinute;
    private int endMinute;

    public Alarm(ArrayList<Integer> days, int startHour, int startMinute, int endHour, int endMinute, ArrayList<AlarmType> alarmTypes)
    {
        this.days = new ArrayList<Integer>();
        this.alarmTypes = new ArrayList<AlarmType>();
        setDays(days);
        setStartHour(startHour);
        setStartMinute(startMinute);
        setEndHour(endHour);
        setEndMinute(endMinute);
        setAlarmTypes(alarmTypes);
    }

    @Override
    public String toString()
    {
        String startTime = startHour+":"+(startMinute<10 ? "0" : "")+startMinute;
        String endTime = endHour+":"+(endMinute<10 ? "0" : "")+endMinute;
        return getTypeString()+"\n"+getDayString()+"\n"+startTime+"-"+endTime;
    }

    private String getDayString()
    {
        String dayString = "";
        GregorianCalendar date = new GregorianCalendar();
        for(int i : days)
        {
            date.set(GregorianCalendar.DAY_OF_WEEK, i);
            dayString += (new SimpleDateFormat("E").format(date.getTime()))+" ";

        }
        return dayString;
    }

    private String getTypeString()
    {
        String typeString = "";
        for(AlarmType alarmType : alarmTypes)
        {
            typeString += alarmType.getName()+"   ";
        }
        return typeString;
    }

    public ArrayList<Integer> getDays() {
        return days;
    }

    public void setDays(ArrayList<Integer> days) {
        this.days = days;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    public int getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(int startMinute) {
        this.startMinute = startMinute;
    }

    public int getEndMinute() {
        return endMinute;
    }

    public void setEndMinute(int endMinute) {
        this.endMinute = endMinute;
    }

    public ArrayList<AlarmType> getAlarmTypes()
    {
        return alarmTypes;
    }

    public void setAlarmTypes(ArrayList<AlarmType> alarmTypes)
    {
        this.alarmTypes = alarmTypes;
    }
}
