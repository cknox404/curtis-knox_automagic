package com.cknox_capstone.AutoMagic;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class ScheduleActivity extends Activity {

    public static final String ALARM_FILE = "alarms";
    public static final String PENDING_INTENT_FILE = "pending_intents";
    private ArrayList<Integer> days;
    private ArrayList<AlarmType> alarmTypes;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule);

        TimePicker startTimePicker = (TimePicker)findViewById(R.id.startTimePicker);
        TimePicker endTimePicker = (TimePicker)findViewById(R.id.endTimePicker);

        startTimePicker.setIs24HourView(true);
        endTimePicker.setIs24HourView(true);

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        int currentHour = gregorianCalendar.get(GregorianCalendar.HOUR_OF_DAY);
        startTimePicker.setCurrentHour(currentHour);
        endTimePicker.setCurrentHour(currentHour);

        days = new ArrayList<Integer>(7);
        alarmTypes = new ArrayList<AlarmType>(5);
    }

    public void onCheckboxClicked(View v)
    {
        boolean checked = ((CheckBox)v).isChecked();
        Integer tag = Integer.parseInt((String)v.getTag());
        if(checked)
            days.add(tag);
        else
            days.remove(tag);
    }

    public void onTypeBoxClicked(View v)
    {
        boolean checked = ((CheckBox)v).isChecked();
        String text = ((CheckBox) v).getText().toString();

        for(AlarmType alarmType : AlarmType.values())
        {
            if(alarmType.equalsName(text))
            {
                if(checked)
                    alarmTypes.add(alarmType);
                else
                    alarmTypes.remove(alarmType);

                break;
            }
        }
    }

    public void createSchedule(View v)
    {
        TimePicker startTimePicker = (TimePicker)findViewById(R.id.startTimePicker);
        TimePicker endTimePicker = (TimePicker)findViewById(R.id.endTimePicker);

        int startHour = startTimePicker.getCurrentHour();
        int startMinute = startTimePicker.getCurrentMinute();

        int endHour = endTimePicker.getCurrentHour();
        int endMinute = endTimePicker.getCurrentMinute();

        for(int day : days)
        {
            for(AlarmType alarmType : alarmTypes)
            {
                Intent intent = new Intent();
                int typeValue = 0;
                switch (alarmType)
                {
                    case SILENT:
                        intent = new Intent(this, SilentScheduleReceiver.class);
                        typeValue = AlarmType.SILENT.getTypeValue();
                        break;
                    case WIFI_OFF:
                        intent = new Intent(this, WifiScheduleReceiver.class);
                        typeValue = AlarmType.WIFI_OFF.getTypeValue();
                        break;
                    case DATA_OFF:
                        intent = new Intent(this, DataScheduleReceiver.class);
                        typeValue = AlarmType.DATA_OFF.getTypeValue();
                        break;
                }
                createAlarm(day, startHour, startMinute, true, intent, typeValue);
                createAlarm(day, endHour, endMinute, false, intent, typeValue);
            }

        }

        Alarm newAlarm = new Alarm(days, startHour, startMinute, endHour, endMinute, alarmTypes);
        saveAlarm(newAlarm);

        Toast.makeText(this, "Schedule created", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, MyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected long getTime(int day, int hour, int minute)
    {
        GregorianCalendar date = new GregorianCalendar();
        date.set(GregorianCalendar.SECOND, 0);
        date.set(GregorianCalendar.MILLISECOND, 0);
        int dayDiff = day - date.get(GregorianCalendar.DAY_OF_WEEK);
        int hourDiff = hour - date.get(GregorianCalendar.HOUR_OF_DAY);
        int minuteDiff = minute - date.get(GregorianCalendar.MINUTE);
        if(dayDiff < 0)
        {
            dayDiff+=7;
        }
        else if(dayDiff == 0)
        {
            if(hourDiff < 0)
            {
                dayDiff+=7;
            }
            else if(hourDiff == 0)
            {
                if(minuteDiff < 1)
                {
                    dayDiff+=7;
                }
            }
        }
        date.add(Calendar.DAY_OF_WEEK, dayDiff);
        date.add(Calendar.HOUR_OF_DAY, hourDiff);
        date.add(Calendar.MINUTE, minuteDiff);

        return date.getTimeInMillis();
    }

    public ArrayList<Alarm> getAlarms()
    {
        ArrayList<Alarm> alarms;
        try{
            FileInputStream fileIn = openFileInput(ALARM_FILE);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            alarms = (ArrayList<Alarm>)objectIn.readObject();

            objectIn.close();
            fileIn.close();
        }catch (Exception e){
            alarms = new ArrayList<Alarm>();
        }
        return alarms;
    }

    private void saveAlarm(Alarm newAlarm)
    {
        ArrayList<Alarm> alarms = getAlarms();

        alarms.add(newAlarm);

        try{
            FileOutputStream fileOut = openFileOutput(ALARM_FILE, MODE_PRIVATE);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(alarms);

            objectOut.close();
            fileOut.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    protected void savePendingIntentId(int id, int day, int hour, int minute, int typeValue)
    {
        HashMap<Integer, Integer> pendingIntentIds;
        try{
            FileInputStream fileIn = openFileInput(PENDING_INTENT_FILE);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            pendingIntentIds = (HashMap<Integer, Integer>)objectIn.readObject();

            objectIn.close();
            fileIn.close();
        }catch (Exception e){
            pendingIntentIds = new HashMap<Integer, Integer>();
        }
        int key = typeValue + (day*24*60) + (hour*60) + minute;
        if(pendingIntentIds.get(key)==null)
        {
            pendingIntentIds.put(key, id);

            try{
                FileOutputStream fileOut = openFileOutput(PENDING_INTENT_FILE, MODE_PRIVATE);
                ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
                objectOut.writeObject(pendingIntentIds);

                objectOut.close();
                fileOut.close();
            }catch (Exception e){
                e.printStackTrace();
            }
            SharedPreferences preferences = getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("CurrentIntentID", id+1);
            editor.commit();
        }
        else
        {

        }
    }

    protected void createAlarm(int day, int hour, int minute, boolean isStart, Intent intent, int typeValue)
    {
        long time = getTime(day, hour, minute);
        intent.putExtra("isAlarmStart", isStart);

        SharedPreferences preferences = getSharedPreferences(SilentTimerActivity.MY_PREFS, Context.MODE_PRIVATE);
        int id = preferences.getInt("CurrentIntentID", 0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, AlarmManager.INTERVAL_DAY * 7, pendingIntent);

        savePendingIntentId(id, day, hour, minute, typeValue);
    }

}